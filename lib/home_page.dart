import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  static String tag = 'home-page';

  @override
  Widget build(BuildContext context) {
    final avatar = Hero(
      tag: 'hero',
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 72.0,
          child: Image.asset('assets/Contact.png'),
        ),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(16.0),
      child: Text(
        'Welcome!',
        style: TextStyle(
          color: Color.fromRGBO(15, 45, 83, 1),
          fontSize: 28.0,
        ),
      ),
    );

    final lipsum = Padding(
      padding: EdgeInsets.all(16.0),
      child: Text(
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum metus ac magna ultrices luctus. Integer tempus lorem urna, ut vestibulum massa porttitor et. Nunc vestibulum in tortor eget pulvinar. Aenean eu mi et leo venenatis euismod. Sed aliquet porttitor tellus, non laoreet tellus lacinia quis. Aenean efficitur lacus at tortor consequat sagittis.!',
        style: TextStyle(
          color: Color.fromRGBO(15, 45, 83, 1),
          fontSize: 16.0,
        ),
      ),
    );

    return Scaffold(
      body: Center(
        child: ListView(children: <Widget>[
          avatar,
          SizedBox(
            height: 16,
          ),
          welcome,
          SizedBox(
            height: 8,
          ),
          lipsum
        ]),
      ),
    );
  }
}
