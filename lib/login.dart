import 'package:flutter/material.dart';
import 'package:zum/home_page.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final airplane = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 40.0,
        child: RotatedBox(
          quarterTurns: 1,
          child: Image.asset('assets/Airplane.png'),
        ),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'user@example.tld',
        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24.0),
        ),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'password',
        contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24.0),
        ),
      ),
    );

    final submitButton = MaterialButton(
      minWidth: 200.0,
      height: 42.0,
      onPressed: () {
        Navigator.of(context).pushNamed(HomePage.tag);
      },
      color: Color.fromRGBO(15, 45, 83, 1),
      child: Text(
        'Iniciar sesion',
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Recordar clave',
        style: TextStyle(
          color: Color.fromRGBO(15, 45, 83, 1),
        ),
      ),
      onPressed: () {},
    );

    return Scaffold(
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24, right: 24),
          children: <Widget>[
            airplane,
            SizedBox(
              height: 48,
            ),
            email,
            SizedBox(
              height: 8,
            ),
            password,
            SizedBox(
              height: 24,
            ),
            submitButton,
            forgotLabel,
          ],
        ),
      ),
    );
  }
}
